import {
  IsNotEmpty,
  IsPositive,
  MinLength,
  IsNumber,
  IsString,
} from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsPositive()
  @IsNumber()
  price: number;
}
